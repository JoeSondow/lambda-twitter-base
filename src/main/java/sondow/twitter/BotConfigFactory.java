package sondow.twitter;

import twitter4j.conf.Configuration;
import twitter4j.conf.ConfigurationBuilder;

/**
 * Builds a BotConfig based on environment variables.
 */
public class BotConfigFactory {

    private Environment environment;

    public BotConfigFactory(Environment environment) {
        this.environment = environment;
    }

    public BotConfigFactory() {
        this(new Environment());
    }

    public BotConfig configure() {
        Configuration twitterConf = configureTwitter();
        return new BotConfig(twitterConf);
    }

    private Configuration configureTwitter() {
        return configureTwitter(null);
    }

    /**
     * AWS Lambda only allows underscores in environment variables, not dots, so the default ways
     * twitter4j finds keys aren't possible. Instead, this custom code gets the configuration from
     * a Lambda-friendly environment variable.
     *
     * @return configuration containing AWS and Twitter authentication strings and other variables
     */
    private Configuration configureTwitter(String prefix) {

        // Override with a specific account if available. This mechanism allows us to provide
        // multiple key sets in the AWS Lambda configuration, and switch which Twitter account
        // to target by retyping just the target Twitter account name in the configuration.
        ConfigurationBuilder configBuilder = new ConfigurationBuilder();
        String key = (prefix == null) ? "twitter_credentials" : prefix + "_twitter_credentials";
        String credentialsCsv = environment.require(key);

        String[] tokens = credentialsCsv.split(",");
        String screenName = tokens[0];
        String consumerKey = tokens[1];
        String consumerSecret = tokens[2];
        String accessToken = tokens[3];
        String accessTokenSecret = tokens[4];

        configBuilder.setUser(screenName);
        configBuilder.setOAuthConsumerKey(consumerKey);
        configBuilder.setOAuthConsumerSecret(consumerSecret);
        configBuilder.setOAuthAccessToken(accessToken);
        configBuilder.setOAuthAccessTokenSecret(accessTokenSecret);

        return configBuilder.setTrimUserEnabled(true).setTweetModeExtended(true).build();
    }
}
