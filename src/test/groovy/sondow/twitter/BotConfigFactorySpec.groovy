package sondow.twitter

import org.junit.Rule
import org.junit.contrib.java.lang.system.EnvironmentVariables
import spock.lang.Specification
import twitter4j.conf.Configuration

class BotConfigFactorySpec extends Specification {

    @Rule
    public final EnvironmentVariables envVars = new EnvironmentVariables()

    def "configure should populate a configuration from environment variable"() {
        setup:
        Keymaster keymaster = Mock()
        Environment environment = new Environment(keymaster)
        BotConfigFactory factory = new BotConfigFactory(environment)
        String filler = Environment.SPACE_FILLER
        envVars.set("twitter_credentials", "${filler}cartoons,bartsimpson,snaggletooth," +
                "fredflintstone,bugsbunny${filler}")

        when:
        BotConfig overallConfig = factory.configure()
        Configuration twitterConfig = overallConfig.getTwitterConfig()

        then:
        with(twitterConfig) {
            OAuthConsumerKey == 'bartsimpson'
            OAuthConsumerSecret == 'snaggletooth'
            OAuthAccessToken == 'fredflintstone'
            OAuthAccessTokenSecret == 'bugsbunny'
        }
    }
}
